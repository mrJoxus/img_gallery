// ich muss mit dem bilder array und den richtigen request URLs gefüttert werden 

const urlMG2017 = "https://192.168.3.10:30000/mgarten/2017/photobox/imgs"
const urlAMG2017 = "https://192.168.3.10:30000/mgarten/2017/cam/imgs"
const urlMG2018 = "https://192.168.3.10:30000/mgarten/2018/photobox/imgs"
const urlBase = "https://192.168.3.10:30000/mgarten"

var url;
var index = {
  start: 0,
  end: 0,
  big: 0
}
var imgsObj,
  year,
  asdf;

function makeRequest(url) {
  ////console.log("makerequest")
  return new Promise(function (resolve, reject) {
    var request = new XMLHttpRequest();
    request.onload = () => {
      if (request.status !== 200 && request.status !== 304) {
        //console.log("reject")

        return reject(request.status);
      } else {
        //console.log("resolve")
        return resolve(request.responseText);
      }
    }
    request.open("GET", url, true)
    request.setRequestHeader("authorization", "Bearer " + token)
    request.send()
  });
}

function buildRequestArray(imgsObj, size) {
  let array = []
  let absSize;
  let id;
  if (size > 0) { //forward
    id = index.end
    if (size + index.end > imgsObj.length) { //size to big, reduce size
      size = imgsObj.length - index.end
      //console.log("size", size)
    }
    index.end = index.end + size
  }

  else { //backward  
    if (index.start < Math.abs(size)) { //size to big, reduce size
      size = - index.start
    }
    index.start = index.start + size
    id = index.start
  }

  absSize = Math.abs(size);

  for (let i = 0; i < absSize; i++) {
    array.push(imgsObj[id])
    id++
    ////console.log("id", id)
  }
  //console.log("array", array)
  return array
}

function wrapImgsInHTMLCode(array) {
  //console.log("wrapImgsInHTMLCode(imgArray)")
  let domObjects = "";
  for (let i = 0, len = array.length; i < len; i++) {

    let rand = ((Math.random() * 10) + 1);
    if (rand >= 10) {
      domObjects = domObjects + appendBigImg(array[i])
      getImgBase64('[alt="' + array[i] + '"]', array[i], "thumb/400")
    }
    else {
      domObjects = domObjects + appendSmallImg(array[i]);
      getImgBase64('[alt="' + array[i] + '"]', array[i], "thumb/200")
    }
  }
  return domObjects
}

// make url more adaptable
function getImgBase64(selector, currentImg, imgSize) {
  makeRequest(`${urlBase}/${year}/${asdf}/${imgSize}/${currentImg}`).then((data) => {
    returnObj = JSON.parse(data);
    $(selector).attr("src", "data:image/png;base64," + returnObj.imageBase64)
  }).catch((error) => {
    //console.log("error getImgbase64", error);
  });
}

function appendSmallImg(imgName) {
  let time = getImgTime(imgName)
  return templSmallImg(time, imgName)
}

function appendBigImg(imgName) {
  let time = getImgTime(imgName)
  return templBigImg(time, imgName)
}

function getImgTime(imgName) {
  let date = imgName.split("T")[0]
  let time = imgName.split("T")[1]
  let cTime = {
    day: date.split("-")[2],
    month: date.split("-")[1],
    year: date.split("-")[0],
    hour: time.split(":")[0],
    minute: time.split(":")[1],
    second: time.split(":")[2]
  }
  cTime.second = cTime.second.replace(".jpg", "")
  if (cTime.second.indexOf("-") > 0) {
    cTime.second = cTime.second.split("-")[0]
  }

  return cTime
}

function setTimeInput() {
  let time = getImgTime(imgsObj[0])
  let endTime = getImgTime(imgsObj[imgsObj.length - 1])
  $("#input-time").val(time.hour + ":" + time.minute)
  // //console.log(time.hour + ":" + time.minute)
  // //console.log(time.year + ":" + time.month + ":" + time.day)
  $("#input-date").val(time.year + "-" + time.month + "-" + time.day)
  $("#input-date").attr("min", time.year + "-" + time.month + "-" + time.day)
  $("#input-date").attr("max", endTime.year + "-" + endTime.month + "-" + endTime.day)
}

function timeInput() { // load pictre from time input time
  $(document).scrollTop("130")
  let imgName = $("#input-date").val() + "T" + $("#input-time").val()
  let t = getImgTime(imgName)
  t.year + t.month + t.day + "T" + t.hour

  for (let i = 0, len = imgsObj.length; i < len; i++) {
    if (t.day + "T" + t.hour == imgsObj[i].slice(8, 13)) {
      if (Number(t.minute) <= Number(imgsObj[i].slice(14, 16))) {
        index.start = i
        index.end = i

        break
      }
    }
    else if (i == len - 1) {
      let number = Number(t.hour) + 1
      if (number < 10) {
        t.hour = "0" + String(number)
      }
      else {
        t.hour = String(number)
      }
      for (let i = 0, len = imgsObj.length; i < len; i++) {
        if (t.day + "T" + t.hour == imgsObj[i].slice(8, 13)) {
          index.start = i
          index.end = i
          break
        }
      }
    }
  }

  let array = buildRequestArray(imgsObj, 50)
  $(".grid").html(wrapImgsInHTMLCode(array))
}

function loadTop() {
  $(window).scroll(function () {
    if ($(window).scrollTop() <= 0) {
      if (index.start != 0) {
        //console.log("index.start", index.start)
        let array = buildRequestArray(imgsObj, -10)
        $(".grid").prepend(wrapImgsInHTMLCode(array))
        $(document).scrollTop("130")
      }
    }
  })
}

function bigPictureOnClick() {
  $('.grid').on('click', 'img', function (e) {
    e.preventDefault();
    $(".modal").addClass("modal--active")
    $("body").addClass("no-scroll")
    let imgAlt = $(this).attr("alt")
    getImgBase64(".modal__img", imgAlt, "img")
    $(".modal__img").attr("alt", imgAlt)
    let imgSrc = $(".modal__img").attr("src")
    $("#download").attr("href", `${imgSrc}`)
    $("#download").attr("download", imgAlt)
    index.big = imgsObj.indexOf(imgAlt)
    let time = getImgTime(imgAlt)
    $("#current-time").text(time.hour + ":" + time.minute + ":" + time.second)
  });
}

function modalControls() {

  $(document).keydown(function (e) {
    if (e.keyCode === 27) {
      clickExit()
    }
  });

  $("#exit").click(function () { clickExit() });

  $(".modal__bg").click(function () { clickExit() });

  $(document).keydown(function (e) {
    if (e.keyCode === 39) {
      let direction = 1
      clickNextPrevious(direction)
    }
  });
  $("#next").click(function () {
    let direction = 1
    clickNextPrevious(direction)
  });

  $(document).keydown(function (e) {
    if (e.keyCode === 37) {
      let direction = -1
      clickNextPrevious(direction)
    }
  });
  $("#previous").click(function () {
    let direction = -1
    clickNextPrevious(direction)
  });
}

function clickExit() {
  $(".modal--active").removeClass("modal--active")
  $("body").removeClass("no-scroll")
}

function clickNextPrevious(direction) {
  index.big = index.big + direction
  imgAlt = imgsObj[index.big]
  let time = getImgTime(imgAlt)
  $("#current-time").text(time.hour + ":" + time.minute + ":" + time.second)

  getImgBase64(".modal__img", imgAlt, "img")
  $(".modal__img").attr("alt", imgAlt)
}

let token = getCookie("mgtoken")


// von pages.html 

// imgsObj = JSON.parse(await makeRequest(url));
// let array = buildRequestArray(imgsObj, 50)
// $(".grid").append(wrapImgsInHTMLCode(array))
// setTimeInput()
// bigPictureOnClick()
// modalControls()
// loadTop()
// $(document).on('keypress', function (e) {
//   if (e.keyCode === 13) {
//     timeInput()
//   }
// });
// $("#time-submit").click(function () {
//   timeInput()
// });