var ulrBase = "https://localhost:8080/mgarten"


// urls von gallery.js kopieren und besser aufteilen

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}


function makeBasicRequest(url) {
  $("#password").removeClass("input-wrong")
  return new Promise(function (resolve, reject) {
    var request = new XMLHttpRequest();
    request.onload = () => {
      if (request.status !== 200 && request.status !== 304) {
        return reject(request);
      }
      else {
        return resolve(request.responseText);
      }
    }
    request.open("GET", url, true)
    request.send()
  });
}


function makeExtendedRequest(url) {
  $("#password").removeClass("input-wrong")
  return new Promise(function (resolve, reject) {
    var request = new XMLHttpRequest();

    request.onload = () => {
      if (request.status !== 200 && request.status !== 304) {
        return reject(request);
      }
      else {
        return resolve(request.responseText);
      }
    }
    request.open("GET", url, true);
    //wait on open event instead
    request.setRequestHeader("authorization", "Bearer " + getCookie('mgtoken'));
    request.send()
  });
}