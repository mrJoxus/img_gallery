function templSmallImg(time, imgName) {
  return `<div class="item"><img alt="${imgName}">
  <div class="item__overlay">
    <div class="clock img-time">
    <div class="center">
    <span>${time.hour}:${time.minute}</span>
      </div>
    </div>
  </div>
</div>`
}

function templBigImg(time, imgName) {
  return `<div class="item" style="grid-area:span 2 / span 2;">
  <img alt="${imgName}">
<div class="item__overlay">
  <div class="clock img-time">
  <div class="center">
  <span>${time.hour}:${time.minute}</span>
    </div>
  </div>
</div>
</div>`
}




function templPages() {
  return `<div class="pages">
  <div class="center">
    <ul class="pages__list">
    <li id="aufbau-mg-2017" class="pages__element page__visible">
    <h1>BeforeWhileAfter 2017</h1>
  </li>
      <li id="mg-2017" class="pages__element page__visible">
        <h1>2017</h1>
      </li>
      <li id="mg-2018" class="pages__element page__visible">
        <h1>2018</h1>
      </li>
    </ul>
  </div>
</div>`
}

function templGallery() {
  return `
  <div class="gallery">
  <i class="back fas fa-angle-left"></i>
  <div class="clock clock--fixed">
  <div class="input-wrapper">
    <span class="clock__filter">Zeit-Filter:</span>
    <input id="input-time" class="input" type="time">
    <input id="input-date" class="input" type="date">
    <i id="time-submit" class="fas fa-search"></i>
  </div>
  </div>
  <div class="wrap">
  
  <div id="grid" class="grid">
  </div>
  </div>

  <div class="modal">
  <div class="modal__bg"></div>
  <img class="modal__img modal__img--active" src="" alt="">
  <div class="modal__info">
  <i id="previous" class="fas fa-arrow-left"></i>
  <i id="exit" class="fas fa-times"></i>
  <i id="next" class="fas fa-arrow-right"></i>
  <div id="current-time"></div>
  </div>
  </div>`
}
