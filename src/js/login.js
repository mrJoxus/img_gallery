const pwUrl = ulrBase + '/register?password='

//login on Click
$("button").click(loginEventHandler);
//login on enter
$(document).on('keypress', (e) => {
  if (e && e.keyCode === 13) {
    loginEventHandler();
  }
});

function loginEventHandler() {
  let password = $("#password").val()
  makeBasicRequest(pwUrl + password).then((response) => {
    token = JSON.parse(response);
    document.cookie = "mgtoken=" + token.token;
    makeExtendedRequest(ulrBase + '/login').then((html) => {
      $("html").html(html);
    }).catch((error) => {
      console.log(error);
      console.error('Error:', error.status, error.responseText);
    });
    //redirect(token) // will not work (helper.js)
  }).catch((error) => {
    $("#password").addClass("input-wrong");
    $("#password").val("");
    if (error.status === 401) {
      console.log('Wrong Password');
    } else {
      console.error('Error :', error.status, error.responseText);
    }
  });
}